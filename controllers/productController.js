import product from "../models/Product.js";
import { check, validationResult } from "express-validator";

export const getProducts = async (req, res) => {
    try {
        const products = await product.findAll()
        res.send(products)
    } catch (error) {
        console.error(error);
    }
}

export const getProductById = async (req, res) => {
    try {
        const data = await product.findAll({
            where : {
                id : req.params.id
            }
        })
        res.json({
            "data" : data
        })
    } catch (error) {
        console.error(error);
    }
}

export const validate = (method) => {
    switch (method) {
        case 'createProduct': {
            return [
                check('title').optional().isLength({ min: 3 }),
                check('price').optional().isInt()
            ]
        }
        case 'updateProduct' : {
            return [
                check('title').optional().isLength({ min: 3 }),
                check('price').optional().isInt()
            ]
        }
    }
}

export const createProduct = async (req, res, next) => {
    try {
        const errors = validationResult(req)

        if(!errors.isEmpty()) {
            return res.status(400).json({
                errors: errors.array() 
            })
        }

        const data = await product.create({
            title : req.body.title,
            price : req.body.price
        })

        res.status(200).json({
            "data" : data,
            "message" : "Product Created"
        })
        
    } catch (error) {
        return next(error)
    }
}

export const updateProduct = async (req, res, next) => {
    try {
        const errors = validationResult(req)

        if(!errors.isEmpty()) {
            return res.status(400).json({
                errors : errors.array()
            })
        }

        const { title, price } = req.body
        
        await product.update(req.body, {
            where : {
                id : req.params.id,
            },
        })
        
        res.status(200).json({
            "data" : {
                "id" : req.params.id,
                "title" : title,
                "price" : price
            },
            "message" : "Product Updated"
        })
    } catch (error) {
        return next(error)
    }
}

export const destroyProduct = async (req, res, next) => {
    try {
        await product.destroy({
            where : {
                id : req.params.id
            }
        })

        res.status(200).json({
            "message" : "Product Deleted"
        })
    } catch (error) {
        return next(error)
    }
}