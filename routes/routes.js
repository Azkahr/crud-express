import express from "express"
import multer from "multer";
import { 
    getProducts, 
    getProductById, 
    createProduct, 
    validate, 
    updateProduct, 
    destroyProduct 
} from "../controllers/productController.js"

const router = express.Router()

router.get('/products', getProducts)
router.get('/product/:id', getProductById)
router.post('/products', multer().none(), validate('createProduct'), createProduct)
router.put('/product/:id', multer().none(), validate('updateProduct'), updateProduct)
router.delete('/product/:id', destroyProduct)

export default router