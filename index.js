import express from "express";
import db from "./config/database.js";
import cors from "cors"
import router from "./routes/routes.js";

const app = express()

app.use(cors())

try {
    await db.authenticate()
    console.log('Database connected')
} catch (error) {
    console.error(error);
}

app.use('/api', router)

app.listen(5000, () => console.log('Server running at http://localhost:5000'))